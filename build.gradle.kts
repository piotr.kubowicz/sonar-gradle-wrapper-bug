plugins {
    kotlin("jvm") version "1.8.22"
    id("org.sonarqube") version "4.4.1.3373"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.commons:commons-lang3:3.11")

    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.3")
}

sonar {
    properties {
        property("sonar.projectKey", System.getenv("SONAR_PROJECT_KEY"))
        property("sonar.organization", "my-org")
        property("sonar.host.url", System.getenv("SONAR_HOST"))
        property("sonar.verbose", true)
        property("sonar.branch.name", System.getenv("CI_COMMIT_REF_NAME") ?: "main")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
