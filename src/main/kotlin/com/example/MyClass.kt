package com.example

import org.apache.commons.lang3.StringUtils

class MyClass {
    fun doIt() {
        println(StringUtils.replaceAll("a", "b", "c"))
    }
}
