package com.example

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

class MyClassTest {

    @Test
    fun `test it`() {
        assertDoesNotThrow { MyClass().doIt() }
    }
}
