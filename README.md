Reproduces a bug where Sonar incorrectly forces download of Gradle Wrapper.

https://community.sonarsource.com/t/sonar-gradle-plugin-starts-unwanted-wrapper-download/101891
